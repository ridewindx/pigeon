#pragma once

#include <mutex>


namespace pg {

using namespace std;

struct Ctx {
    mutex mutex_;
};

}
